import { takeLatest, call, put } from "redux-saga/effects";

const FETCH_FACTORS_REQUEST = 'FETCH_FACTORS_REQUEST';
const FETCH_FACTORS_SUCCESS = 'FETCH_FACTORS_SUCCESS';
const FETCH_FACTORS_FAILURE = 'FETCH_FACTORS_FAILURE';

export const fetchFactors = (origin, radius) => ({
  type: FETCH_FACTORS_REQUEST,
  payload: { origin, radius },
});

const fakeFetchFactors = () => new Promise(res => {
  setTimeout(() => res({ data: [1, 2, 3] }), 1200);
});

function* fetchWorker({ payload }) {
  try {
    const { data } = yield call(fakeFetchFactors, payload);
    yield put({ type: FETCH_FACTORS_SUCCESS, data });
  } catch (error) {
    yield put({ type: FETCH_FACTORS_FAILURE, error });
  }
}

export function* factorsWatcher() {
  yield takeLatest(FETCH_FACTORS_REQUEST, fetchWorker);
}

const initialState = {
  fetching: false,
  data: [],
  error: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FACTORS_REQUEST:
      return { ...state, fetching: true, error: null };

    case FETCH_FACTORS_SUCCESS:
      return { ...state, fetching: false, data: action.data };

    case FETCH_FACTORS_FAILURE:
      return { ...state, fetching: false, data: [], error: action.error };

    default:
      return state;
  }
}
