const MAP_CENTER_CHANGED = 'MAP/CENTER_CHANGED';
const MAP_ZOOM_CHANGED = 'MAP/ZOOM_CHANGED';

export const getCenter = state => state.map.center;
export const getZoom = state => state.map.zoom;

export const mapCenterChanged = center => ({
  type: MAP_CENTER_CHANGED,
  payload: { center },
});

export const mapZoomChanged = zoom => ({
  type: MAP_ZOOM_CHANGED,
  payload: { zoom },
});

const WINNIPEG_CENTER = { lat: 49.8636253, lng: -97.2479175 };
const DEFAULT_ZOOM = 12;

export const initialState = {
  center: WINNIPEG_CENTER,
  zoom: DEFAULT_ZOOM,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case MAP_CENTER_CHANGED:
      return { ...state, center: action.payload.center };

    case MAP_ZOOM_CHANGED:
      return { ...state, zoom: action.payload.zoom }

    default:
      return state;
  }
}
