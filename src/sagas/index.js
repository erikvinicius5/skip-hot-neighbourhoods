import createSagaMiddleware from "redux-saga";
import { createStore, applyMiddleware, combineReducers } from "redux";

import factorsReducer, { factorsWatcher } from './factors';
import mapReducer from './map';

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  combineReducers({
    factors: factorsReducer,
    map: mapReducer,
  }),
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(factorsWatcher);
