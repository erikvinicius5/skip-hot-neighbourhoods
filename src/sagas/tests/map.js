import reducer, { mapCenterChanged, mapZoomChanged, initialState } from '../map';

describe('Map Reducer', () => {
  it('should return initial state for unrelated action', () => {
    expect(reducer(initialState, { type: 'UNRELATED_ACTION' })).toBe(initialState);
  });

  it('should change center when mapCenterChanged', () => {
    const newCenter = { lat: 10, lgn: 20 };
    const action = mapCenterChanged(newCenter);

    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      center: newCenter,
    });
  });

  it('should change zoom when mapZoomChanged', () => {
    const newZoom = 5;
    const action = mapZoomChanged(newZoom);

    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      zoom: newZoom,
    });
  });
});
