import { buildShallowRenderer } from 'test/utils';

import EnhancedMap, { Map } from '..';

describe('EnhancedMap', () => {
  it('should be properly rendered', () => {
    expect(buildShallowRenderer(EnhancedMap)()).toMatchSnapshot();
  });
});

describe('Map', () => {
  const renderMap = buildShallowRenderer(Map);

  it('should be properly rendered', () => {
    const map = renderMap({
      zoom: 5,
      center: { lat: 10, lng: 20 },
      onDragEnd: jest.fn(),
      onZoomChanged: jest.fn(),
    });
    expect(map).toMatchSnapshot();
  });

  it('should handle position change properly', () => {
    const onChangeCenter = jest.fn();
    const map = renderMap({ onChangeCenter });

    map.instance().googleMapRef = {
      getCenter: () => ({ lat: () => 10, lng: () => 20 }),
    };

    map.simulate('dragEnd');

    expect(onChangeCenter).toHaveBeenCalledWith({ lat: 10, lng: 20 });
  });

  it('should handle zoom change properly', () => {
    const onChangeZoom = jest.fn();
    const map = renderMap({ onChangeZoom });

    map.instance().googleMapRef = { getZoom: () => 5 };

    map.simulate('zoomChanged');

    expect(onChangeZoom).toHaveBeenCalledWith(5);

  });
});
