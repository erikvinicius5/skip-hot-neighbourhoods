import React, { Component } from 'react';
import { GoogleMap, withGoogleMap, withScriptjs } from 'react-google-maps';
import { compose, withProps } from 'recompose';
import { bind } from 'decko';
import { connect } from 'react-redux';

import { getCurrentPosition } from 'src/utils/location';
import { getCenter, getZoom, mapCenterChanged, mapZoomChanged } from 'src/sagas/map';

export class Map extends Component {
  async componentDidMount() {
    const currentPosition = await getCurrentPosition();
    if (currentPosition) {
      this.props.onChangeCenter(currentPosition);
    }
  }

  @bind
  handleDragEnd() {
    const { lat, lng } = this.googleMapRef.getCenter();
    this.props.onChangeCenter({ lat: lat(), lng: lng() });
  }

  @bind
  handleZoomChange() {
    this.props.onChangeZoom(this.googleMapRef.getZoom());
  }

  render() {
    return (
      <GoogleMap
        ref={(node) => { this.googleMapRef = node; }}
        zoom={this.props.zoom}
        center={this.props.center}
        onDragEnd={this.handleDragEnd}
        onZoomChanged={this.handleZoomChange}
        {...this.props}
      />
    );
  }
}

export default compose(
  withProps({
    googleMapURL: process.env.REACT_APP_GOOGLE_MAPS_URL,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: '600px' }} />,
    mapElement: <div style={{ height: '100%' }} />,
  }),
  withScriptjs,
  withGoogleMap,
  connect(
    state => ({ center: getCenter(state), zoom: getZoom(state) }),
    dispatch => ({
      onChangeCenter: center => dispatch(mapCenterChanged(center)),
      onChangeZoom: zoom => dispatch(mapZoomChanged(zoom)),
    }),
  ),
)(Map);
