export const getCurrentPosition = () => new Promise(res => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(({ coords }) => {
      res({ lat: coords.latitude, lng: coords.longitude });
    });
  } else {
    res(null);
  }
});
