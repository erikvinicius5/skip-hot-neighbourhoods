import React, { Component } from 'react';
import { connect } from 'react-redux';

import Map from 'src/components/map';
import { fetchFactors } from 'src/sagas/factors';

class App extends Component {
  componentDidMount() {
    this.props.fetchFactors([-23.5817919,-46.7025872], 10);
  }

  render() {
    return (
      <div className="App">
        <Map />
      </div>
    );
  }
}

export default connect(
  state => state,
  dispatch => ({
    fetchFactors: (origin, radius) => dispatch(fetchFactors(origin, radius)),
  }),
)(App);
