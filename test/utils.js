import React from 'react';
import { shallow } from 'enzyme';

export const buildShallowRenderer = (Component) => (props) =>
  shallow(<Component {...props} />);
